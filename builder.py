#!/usr/bin/env python2
import os
import logging
import subprocess
import time

REPO_ROOT = os.path.dirname(os.path.realpath(__file__))


def print_stage(line):
    # type: (str) -> None
    input_len = len(line)
    begin = 24 - input_len / 2
    if (input_len % 2) == 0:
        end = 24 - input_len / 2
    else:
        end = 24 - input_len / 2 - 1
    print("=" * 50)
    print("=" + " " * 48 + "=")
    print("=" + " " * int(begin) + line + " " * int(end) + "=")
    print("=" + " " * 48 + "=")
    print("=" * 50)


def print_line(line):
    # type: (str) -> None
    print("=" * 50)
    print(" " * int(25 - len(line) / 2) + line)
    print("=" * 50)


# =============================
#
# Configuration
#
# =============================
IMAGE_OUTPUT_QCOW2 = "test.qcow2"  # type: str
IMAGE_OUTPUT_IMG = "test.img"  # type: str
COMPRESSED_IMAGE_OUTPUT_QCOW2 = "test.qcow2.xz"  # type: str
COMPRESSED_IMAGE_OUTPUT_IMG = "test.img.xz"  # type: str

IMAGE_SIZE = 4194304000  # type: int

CONFIG_OVERLAY = "test_overlay"  # type: str
ROOTFS = "/mnt/test"  # type: str

PARTITION_LIST = ["p1"]  # type: list
PARTITION1_START = 0
PARTITION1_END = 0
PARTITION1_SIZE = 0

INSTALL_PACKAGE_LIST = """kernel sudo kernel bash NetworkManager grub2-pc net-tools lsof dmidecode ethtool 
openssh-server openssh-clients openssl pciutils vim-common nmap-ncat chrony"""  # type: str

INSTALL_EXCLUDE_PACKAGES = ""  # type: str
SYSTEMD_ENABLE_SERVICES = "chrony"  # type: str
SYSTEMD_DISABLE_SERVICES = "systemd-readahead-collect systemd-readahead-replay NetworkManager-wait-online"  # type: str

LO_DEVICE = ""  # type: str

# array of commands to run at completion of the script. These are simple os.system calls
# add by appending. On completion script will pop and run.
exit_commands = ["echo 'Done running exit commands.'"]  # type: list


# =============================
#
# End of Configuration
#
# =============================
def install(install_package_list, exclude_packages, root_filesystem):
    # type: (str, str, str) -> None
    print_line("Yum installing packages into image")
    logging.debug("Rebuilding rpm database for root filesystem")
    os.system("rpm --rebuilddb --root={0}".format(root_filesystem))
    logging.info("yum repos enabled")
    os.system("yum repolist")

    logging.debug("Creating list of excluded packages")
    exclude_list = ""  # type: str
    for item in exclude_packages.split(" "):
        exclude_list = " --exclude=" + item

    logging.info("Disk free space available for new image")
    os.system("df -lh ")
    for package in install_package_list.split(" "):
        os.system(
            "yum -y --setopt=tsflags=nodocs --installroot=" + root_filesystem + " " + exclude_list + " install " + package)
    logging.info("Disk free space after installing packages")
    os.system("df -lh ")
    logging.debug("Done installing packages into image.")


def copy_config_overlay(repo_root, config_overlay, root_file_system):
    # type: (None) -> None
    print_line("Copying configuration overlay into image")
    logging.debug("Copying " + config_overlay + " into the image.")
    os.system("cp -vR " + repo_root + "/" + config_overlay + "/* " + root_file_system + " 2>/dev/null || :")
    logging.debug("Done copying config overlay into image.")


def check_rhel_mounted():
    if not os.path.isfile("/mnt/rhel/repodata/repomd.xml"):
        os.system("mount /dev/sr0 /mnt/rhel")


def check_img_size(size):
    # type: (int) -> bool
    print_line("Checking image size")
    output = size % 512
    if output != 0:
        logging.error("The image size is " + str(size) + " and must be mod 512.")
        return False
    else:
        logging.info("Checked image to be mod 512.")
        return True


def fallocate_disk(image_size, repo_root, image_output_img):
    # type: (int, str, str) -> None
    print_line("Allocate size:" + str(image_size) + " for img file:" + image_output_img)
    logging.debug("fallocate -l {0} {1}/{2}".format(str(image_size), repo_root, image_output_img))
    os.system("fallocate -l {0} {1}/{2}".format(str(image_size), repo_root, image_output_img))
    logging.debug("Done allocating disk space for the new image.")


def mount_lo_device(root_file_system, image_output_img, repo_root, partitions):
    # type: (str, str, str, str, list) -> str
    print_line("Mount loopback device and proc, sys, dev onto it")
    if not os.path.isdir(root_file_system):
        os.makedirs(root_file_system)

    # TODO: add support for multiple partitions with different partition types
    print_line("Create partition on img file for partition: " + partitions[0])

    logging.debug("parted -s {0}/{1} mklabel msdos".format(repo_root, image_output_img))
    os.system("parted -s {0}/{1} mklabel msdos".format(repo_root, image_output_img))

    logging.debug("parted -s {0}/{1} mkpart primary 1MiB 3GiB".format(repo_root, image_output_img))
    os.system("parted -s {0}/{1} mkpart primary 1MiB 3GiB".format(repo_root, image_output_img))

    logging.debug("Listing newly created partitions in new disk. fdisk -l {0}/{1}".format(repo_root, image_output_img))
    os.system("fdisk -l {0}/{1}".format(repo_root, image_output_img))

    cmd = "losetup -fP --show {0}/{1}".format(repo_root, image_output_img)
    logging.debug(cmd)

    new_lo_device = subprocess.check_output(cmd, shell=True).rstrip()  # type: str
    logging.debug("mkfs.ext4 {0}{1}".format(new_lo_device, partitions[0]))
    os.system("mkfs.ext4 {0}{1}".format(new_lo_device, partitions[0]))

    logging.debug("mount {0}{1} {2}".format(new_lo_device, partitions[0], root_file_system))
    os.system("mount {0}{1} {2}".format(new_lo_device, partitions[0], root_file_system))
    exit_commands.append("umount " + root_file_system)
    logging.debug("Adding umount root filesystem to exit commands.")
    logging.debug("New loopback device name is " + new_lo_device)
    return new_lo_device


def mount_dev_proc_sys(root_file_system):
    # type: (str) -> None
    logging.debug("Mount proc, sys and dev")
    if not os.path.isdir(root_file_system + "/proc"):
        os.makedirs(root_file_system + "/proc")
    os.system("mount -t proc /proc " + root_file_system + "/proc")
    exit_commands.append("umount " + root_file_system + "/proc")
    logging.debug("Adding umount proc filesystem to exit commands.")

    if not os.path.isdir(root_file_system + "/dev"):
        os.makedirs(root_file_system + "/dev")
    os.system("mount --rbind /dev " + root_file_system + "/dev")
    os.system("mount --make-rslave " + root_file_system + "/dev")
    exit_commands.append("umount -R " + root_file_system + "/dev")
    logging.debug("Adding umount dev filesystem to exit commands.")

    if not os.path.isdir(root_file_system + "/sys"):
        os.makedirs(root_file_system + "/sys")
    os.system("mount --rbind /sys " + root_file_system + "/sys")
    os.system("mount --make-rslave " + root_file_system + "/sys")
    exit_commands.append("umount -R " + root_file_system + "/sys")
    logging.debug("Adding umount sys filesystem to exit commands.")
    logging.debug("Done temporarily mounting dev, proc, and sys into image.")


def create_grub_entry(lo_device, partition, root_file_system):
    # type: (str, str, str) -> None
    print_line("Create grub menu entry and install grub")

    logging.debug("Getting uuid of blkid -s UUID -o value " + lo_device + partition)
    uuid = subprocess.check_output(
        "blkid -s UUID -o value {0}{1}".format(lo_device, partition), shell=True).rstrip()  # type: str
    logging.debug("UUID from blkid is " + uuid)

    sedcmd = "sed -i 's+SET_UUID+" + uuid + "+g' " + root_file_system + "/etc/grub.d/40_custom"
    logging.debug("sed cmd is: " + sedcmd)
    os.system(sedcmd)
    sedcmd = "sed -i 's+SET_UUID+" + uuid + "+g' " + root_file_system + "/etc/fstab"
    logging.debug("sed cmd is: " + sedcmd)
    os.system(sedcmd)
    logging.debug("Removing 10_linux from image")
    os.system("rm {0}/etc/grub.d/10_linux".format(root_file_system))

    kernels_installed_list = subprocess.check_output(
        "ls {0}/boot/initramfs-* | cut -d'-' -f2- | rev | cut -d'.' -f2- | rev".format(root_file_system), shell=True).split("\n")
    kernel_version = kernels_installed_list[0]
    print("kernel version for guest is: " + kernel_version)
    sedcmd = "sed -i 's+KERNEL_VERSION+" + kernel_version + "+g' " + root_file_system + "/etc/grub.d/40_custom"
    logging.debug("sed cmd is: " + sedcmd)
    os.system(sedcmd)
    logging.debug("After sed commands to modify the custom grub menu entry.")
    logging.debug(subprocess.check_output("cat {0}/etc/grub.d/40_custom".format(root_file_system), shell=True))

    os.system("chroot {0} env -i /usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg".format(root_file_system))
    os.system("chroot {0} env -i /usr/sbin/grub2-install --force {1}".format(root_file_system, lo_device))
    os.system("echo 'root:oce' | chroot " + root_file_system + " /usr/sbin/chpasswd")
    logging.debug("Done creating grub menu entry and installing into image.")


def get_lo_device_info(lo_device, partition):
    # type: (str, str) -> (str, str, str)
    logging.debug("partx {0}{1} -g -o START".format(lo_device, partition))
    partition_start = subprocess.check_output("partx {0}{1} -g -o START".format(lo_device, partition),
                                              shell=True).rstrip()
    logging.debug("partx {0}{1} -g -o END".format(lo_device, partition))
    partition_end = subprocess.check_output("partx {0}{1} -g -o END".format(lo_device, partition), shell=True).rstrip()
    logging.debug(str(int(partition_end) - int(partition_start)))
    partition_size = int(partition_end) - int(partition_start)
    logging.debug("Done getting the loopback device information.")
    logging.debug("return items: " + str(partition_start) + " " + str(partition_end) + " " + str(partition_size))
    return partition_start, partition_end, partition_size


def exit_commands_cleanup():
    while len(exit_commands) > 0:
        command = exit_commands.pop()
        print("Running command: " + command)
        os.system(command)
    logging.debug("Done running the cleanup commands.")


def compress_img_file(img_file_path):
    # type: (str) -> None
    logging.info("Compressing image file to {0}.xz".format(img_file_path))
    os.system("xz -k -T 0 {0}".format(img_file_path))
    logging.debug("Done compressing image.")


def create_qcow2(img_file_path, qcow2_file_path):
    # type: (str, str) -> None
    logging.info("Creating qcow2 from img file")
    os.system("qemu-img convert -O qcow2 {0} {1}".format(img_file_path, qcow2_file_path))
    logging.debug("Done converting img file to qcow2.")


def compress_qcow2(qcow2_file_path):
    # type: (str) -> None
    logging.info("Compressing qcow2 file to {0}.xz".format(qcow2_file_path))
    os.system("xz -k -T 0 {0}".format(qcow2_file_path))
    logging.debug("Done compressing qcow2.")


if __name__ == "__main__":
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.DEBUG,
                        format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename='imagebuildlog.log',
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    print_stage("Welcome!")

    print_stage("Checking all image settings")
    check_img_size(IMAGE_SIZE)
    check_rhel_mounted()

    print_stage("Allocate and install configuration into image")
    fallocate_disk(IMAGE_SIZE, REPO_ROOT, IMAGE_OUTPUT_IMG)
    LO_DEVICE = mount_lo_device(ROOTFS, IMAGE_OUTPUT_IMG, REPO_ROOT, PARTITION_LIST).rstrip()
    mount_dev_proc_sys(ROOTFS)
    install(INSTALL_PACKAGE_LIST, INSTALL_EXCLUDE_PACKAGES, ROOTFS)
    copy_config_overlay(REPO_ROOT, CONFIG_OVERLAY, ROOTFS)
    create_grub_entry(LO_DEVICE, PARTITION_LIST[0], ROOTFS)

    print_stage("Creating final image and qcow2 files and compressing")
    PARTITION1_START, PARTITION1_END, PARTITION1_SIZE = get_lo_device_info(LO_DEVICE, PARTITION_LIST[0])
    compress_img_file(IMAGE_OUTPUT_IMG)
    create_qcow2(IMAGE_OUTPUT_IMG, IMAGE_OUTPUT_QCOW2)
    compress_qcow2(IMAGE_OUTPUT_QCOW2)

    exit_commands.append("losetup -d {0}{1}".format(LO_DEVICE, PARTITION_LIST[0]))

    exit_commands_cleanup()
