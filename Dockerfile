FROM scratch
ADD centos-7-docker.tar.xz /

LABEL org.label-schema.schema-version="1.0" \
    org.label-schema.name="CentOS Base Image" \
    org.label-schema.vendor="CentOS" \
    org.label-schema.license="GPLv2" \
    org.label-schema.build-date="20181204"

#Install git, parted, python, xz
RUN yum update \        
    yum install -y git parted python xz

RUN mkdir /home/vmbuilder \      
           cd /home/vmbuilder \        
           git clone https://invisibleninja92@bitbucket.org/invisibleninja92/vmbuilder.git
           
CMD ["/home/vmbuilder/builder.py"]
