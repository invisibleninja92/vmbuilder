#!/usr/bin/env bash

set -uo pipefail
set -E
trap 'echo >&2 "${BASH_SOURCE[0]}: line ${LINENO}: Error ${?}"' ERR
scriptdir=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
reporoot=${scriptdir}

version=${1:?Missing version as parameter 1}
branch_name=${2:?Missing branch name as parameter 2}

if [[ ${branch_name} == master || ${branch_name} == lts* ]]; then
    build_type="release"
elif [[ $branch_name == release/* ]]; then
    build_type="staging"
elif [[ $branch_name == develop ]]; then
    build_type="develop"
else
    build_type="feature"
fi

echo "Build type is ${build_type}"

function print {
echo "====== $1 ======"
}

function prepend_trap {
    local new_cmd="$1" event="$2"
    local cmd="$(trap -p $event)"
    if [ "$cmd" ]; then
        cmd="${cmd:0:8}$(printf %q "${new_cmd}")';'${cmd:8}"
    else
        cmd="trap -- $(printf %q "${new_cmd}") $event"
    fi
    eval "$cmd"
}

_atexit=("")

function push_atexit {
    _atexit=($# "$@" "${_atexit[@]}")
}

function pop_atexit {
    local nargs=${_atexit[0]}
    "${_atexit[@]:1:$nargs}"
    _atexit=("${_atexit[@]:$((1+$nargs))})")
}

function atexit {
    push_atexit "$@"
    prepend_trap pop_atexit EXIT
}

IMAGE_OUTPUT_QCOW2=test.qcow2
IMAGE_OUTPUT_IMG=test.img
COMPRESSED_IMAGE_OUTPUT_QCOW2=test.qcow2.xz
COMPRESSED_IMAGE_OUTPUT_IMG=test.img.xz

IMAGE_SIZE=1258291200

OVERLAY=test_overlay
rootFS=/mnt/test

PRIMARY_PARTITION_NAME=p1

INSTALL_PACKAGE_LIST="kernel sudo bash NetworkManager grub2-pc net-tools less lsof dmidecode ethtool openssh-server openssh-clients openssl pciutils vim-common nmap-ncat chrony"

INSTALL_EXCLUDE_PACKAGES=""
SYSTEMD_ENABLE_SERVICES="chrony"
SYSTEMD_DISABLE_SERVICES="systemd-readahead-collect systemd-readahead-replay NetworkManager-wait-online"

mkdir -p ${rootFS}

print "allocate for the img file and create partitions"
fallocate -l ${IMAGE_SIZE} ${reporoot}/${IMAGE_OUTPUT_IMG} || exit 1
parted -s ${reporoot}/${IMAGE_OUTPUT_IMG} mklabel msdos || exit 2
parted -s ${reporoot}/${IMAGE_OUTPUT_IMG} "mkpart primary 1MiB -0" || exit 3

print "mount loopback device and create ext4 filesystem"
TEST_LO_DEVICE="$(losetup -fP --show ${reporoot}/${IMAGE_OUTPUT_IMG})"
mkfs.ext4 ${TEST_LO_DEVICE}${PRIMARY_PARTITION_NAME} || exit 4

mount ${TEST_LO_DEVICE}${PRIMARY_PARTITION_NAME} ${rootFS} || exit 5

print "when complete umount rootfs"
atexit umount ${rootFS}

print "mount proc on rootfs"
mkdir -p ${rootFS}/proc
mount -t proc /proc ${rootFS}/proc
atexit umount ${rootFS}/proc

print "mount dev on rootfs"
mkdir -p ${rootFS}/dev
mount --rbind /dev ${rootFS}/dev
mount --make-rslave ${rootFS}/dev
atexit umount -R ${rootFS}/dev

print "mount sys on rootfs"
mkdir -p ${rootFS}/sys
mount --rbind /sys ${rootFS}/sys
mount --make-rslave ${rootFS}/sys
atexit umount -R ${rootFS}/sys

(
    rpm --rebuilddb --root=${rootFS}
    yum repolist
    excludes=""
    for exclude_packages in $INSTALL_EXCLUDE_PACKAGES; do
        excludes+=" --exclude=$exclude_package"
    done
    mount
    df -lh
    for package in ${INSTALL_PACKAGE_LIST}; do
        yum -y --setopt=tsflags=nodocs --installroot=${rootFS} $excludes install $package || exit 66
    done
    df -h
) || exit 6

echo "export PS1=\"[\\u@\h-v${version}-${build_type} \\W]$ \"" >> ${rootFS}/etc/profile.d/sh.local
#mkdir -p ${rootFS}/root/.ssh
#chmod 700 "${rootFS}/root/.ssh"
#chmod 600 "$rootFS/root/.ssh/authorized_keys"

echo "(hd0) ${TEST_LO_DEVICE}" > ${rootFS}/boot/grub2/device.map

UUID=$(blkid ${TEST_LO_DEVICE}${PRIMARY_PARTITION_NAME} | cut -d "\"" -f 2)

#sed -i s/SET_UUID/$UUID/ ${rootFS}/etc/grub.d/40_custom
#sed -i s/SET_UUID/$UUID/ ${rootFS}/etc/fstab

#rm ${rootFS}/etc/grub.d/10_linux

chroot ${rootFS} env -i /usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
chroot ${rootFS} env -i /usr/sbin/grub2-install --force ${TEST_LO_DEVICE}
chroot ${rootFS} echo "root:oce" | chroot ${rootFS} /usr/sbin/chpasswd 

p1Start=$(partx ${TEST_LO_DEVICE}p1 -g -o START)
p1End=$(partx ${TEST_LO_DEVICE}p1 -g -o END)

tacticalStart=0
tacticalEnd=${p1End}
tacticalSize=$((${tacticalEnd} - ${tacticalStart}))

declare -a pids
dd if=${TEST_LO_DEVICE} | xz -k -T 0 > ${COMPRESSED_IMAGE_OUTPUT_IMG} &

pids[0]=$!
wait ${pids[@]}

qemu-img convert -O qcow2 ${IMAGE_OUTPUT_IMG} ${IMAGE_OUTPUT_QCOW2} 
xz -k -T 0 ${IMAGE_OUTPUT_QCOW2}

losetup -d ${TEST_LO_DEVICE}${PRIMARY_PARTITION_NAME}
